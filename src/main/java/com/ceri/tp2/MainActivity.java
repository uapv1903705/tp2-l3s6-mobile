package com.ceri.tp2;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import fr.uavignon.shuet.tp2.data.Wine;
import fr.uavignon.shuet.tp2.data.WineDbHelper;

public class MainActivity extends AppCompatActivity
{

    ListView listView;
    Cursor cursor;
    private WineDbHelper wineDbHelper;
    private MenuItem contextMenuSupprimer;
    private MenuItem contextMenuModifier;

    @Override
    protected void onResume()
    {
        super.onResume();
        updateList();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        wineDbHelper = new WineDbHelper(this);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {   goToWineActivity(-1);    }
        });

        updateList();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {   goToWineActivity(position);    }
        });
        registerForContextMenu(listView);

    }

    public void goToWineActivity(int position)
    {
        Intent appInfo = new Intent(MainActivity.this, WineActivity.class);
        Parcelable wineParcelable = null;
        if(position < 0)    // NEW WINE
        {
            wineParcelable = new Wine(-1,"", "", "", "", "");
        }
        else                // MODIFY WINE
        {
            cursor.moveToPosition(position);
            wineParcelable = WineDbHelper.cursorToWine(cursor);
        }
        appInfo.putExtra("wine", wineParcelable);
        startActivity(appInfo);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        if(v.getId() == R.id.listWine)
        {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
            cursor.moveToPosition(acmi.position);
            contextMenuModifier = menu.add(0, v.getId(), 0, "Modifier");
            contextMenuSupprimer = menu.add(0, v.getId(), 0, "Supprimer");
            menu.setHeaderTitle(cursor.getString(1));
        }
    }
    public boolean onContextItemSelected(MenuItem item)
    {
        if(item == contextMenuModifier)
        {
            goToWineActivity(cursor.getPosition());
            return true;
        }
        else if(item == contextMenuSupprimer)
        {
            try
            {
                wineDbHelper.deleteWine(cursor);
                Toast.makeText(this, "\""+cursor.getString(1) + "\" à été supprimé.", Toast.LENGTH_SHORT).show();
                updateList();
                return true;
            }
            catch(Exception e)
            {
                Toast.makeText(this, "Erreur lors de la suppréssion", Toast.LENGTH_SHORT).show();
            }
            return false;
        }
        return false;
    }

    public void updateList()
    {
        cursor = wineDbHelper.fetchAllWines();
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                R.layout.winelist,
                cursor,
                new String[] {WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION},
                new int[] {R.id.nom, R.id.description}
        );

        listView = (ListView) findViewById(R.id.listWine);
        listView.setAdapter(adapter);
    }

}
