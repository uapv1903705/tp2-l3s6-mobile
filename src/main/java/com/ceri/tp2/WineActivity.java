package com.ceri.tp2;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import fr.uavignon.shuet.tp2.data.Wine;
import fr.uavignon.shuet.tp2.data.WineDbHelper;

public class WineActivity extends AppCompatActivity
{

    EditText wineName;
    EditText editLoc;
    EditText editClimate;
    EditText editPlantedArea;
    EditText editWineRegion;
    WineDbHelper wineDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        Intent intent = getIntent();
        final Wine wineObj = intent.getParcelableExtra("wine");
        final WineDbHelper db = new WineDbHelper(this);

        wineDbHelper = new WineDbHelper(this);

        // --------------------------------------------------
        wineName = findViewById(R.id.wineName);
        wineName.setText(wineObj.getTitle());
        editLoc = findViewById(R.id.editLoc);
        editLoc.setText(wineObj.getLocalization());
        editClimate = findViewById(R.id.editClimate);
        editClimate.setText(wineObj.getClimate());
        editPlantedArea = findViewById(R.id.editPlantedArea);
        editPlantedArea.setText(wineObj.getPlantedArea());
        editWineRegion = findViewById(R.id.editWineRegion);
        editWineRegion.setText(wineObj.getRegion());
        // --------------------------------------------------

        Button saveButton = findViewById(R.id.button);

        saveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(wineName.getText().toString().isEmpty())
                {
                    new AlertDialog.Builder(WineActivity.this).setTitle("Erreur").setMessage("Le nom du vin ne doit pas être vide!").show();
                    return;
                }

                // Check if the name changed to an existing wine, if new, check if it already exists.
                if( (!wineObj.getTitle().trim().equals(wineName.getText().toString().trim()) && wineObj.getId() != -1) || wineObj.getId() == -1)
                {
                    Cursor cursor = wineDbHelper.fetchAllWines();
                    for (int i = 0; i < cursor.getCount(); i++)
                    {
                        cursor.moveToPosition(i);
                        if (cursor.getString(1).trim().equals(wineName.getText().toString().trim()))
                        {
                            new AlertDialog.Builder(WineActivity.this).setTitle("Erreur").setMessage("Le nom donné existe déja dans la base de données").show();
                            return;
                        }
                    }
                }

                Wine wine = new Wine(   wineObj.getId(),
                        wineName.getText().toString(),
                        editWineRegion.getText().toString(),
                        editLoc.getText().toString(),
                        editClimate.getText().toString(),
                        editPlantedArea.getText().toString()
                );

                if(wineObj.getId() < 0)
                    db.addWine(db.getWritableDatabase(), wine);
                else
                    db.updateWine(wine);
                finish();
            }
        });
    }
}
