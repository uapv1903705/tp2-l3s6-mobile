package fr.uavignon.shuet.tp2.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";

    public WineDbHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE "+TABLE_NAME+" ("+_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"+COLUMN_NAME+" TEXT, "+COLUMN_WINE_REGION+" TEXT, "+COLUMN_LOC+" TEXT, "+COLUMN_CLIMATE+" TEXT, "+COLUMN_PLANTED_AREA+" TEXT);");
        populate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }


    /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(SQLiteDatabase db, Wine wine)
    {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_NAME, wine.getTitle());
        cv.put(COLUMN_CLIMATE, wine.getClimate());
        cv.put(COLUMN_LOC, wine.getLocalization());
        cv.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());
        cv.put(COLUMN_WINE_REGION, wine.getRegion());

        // Inserting Row
        long rowID = db.insert(TABLE_NAME, COLUMN_NAME, cv);


        //db.close();

        return (rowID != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */

    public int updateWine(Wine wine)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_NAME, wine.getTitle());
        cv.put(COLUMN_WINE_REGION, wine.getRegion());
        cv.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());
        cv.put(COLUMN_LOC, wine.getLocalization());
        cv.put(COLUMN_CLIMATE, wine.getClimate());

        System.out.println("HELLO THIS IS MICROSOFT TECH SUPPORT HOW CAN I HELP YOU?");

        return db.update(TABLE_NAME, cv, _ID + " = ?", new String[] { Long.toString(wine.getId()) } );
    }

    /**
     * Returns a cursor on all the wines of the library
     */

    public Cursor fetchAllWines()
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME, new String[] {_ID, COLUMN_NAME,
                        COLUMN_WINE_REGION, COLUMN_LOC, COLUMN_CLIMATE, COLUMN_PLANTED_AREA},
                null, null, null, null,
                null, null);

        if (cursor != null)
            cursor.moveToFirst();

        return cursor;
    }

    public void deleteWine(Cursor cursor)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, _ID + " = " + cursor.getLong(0), null);
        //db.close();
    }

    public Cursor getWine(SQLiteDatabase db, Long id)
    {
        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE TRIM("+_ID+") = '"+id+"'", null);
        cursor.moveToFirst();
        return cursor;
    }

    public void populate(SQLiteDatabase db)
    {
        Log.d(TAG, "call populate()");
        addWine(db, new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(db, new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(db, new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(db, new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(db, new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(db, new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(db, new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(db, new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(db, new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(db, new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(db, new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));


        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        //db.close();
    }


    public static Wine cursorToWine(Cursor cursor)
    {
        Wine wine = null;

        wine = new Wine(cursor.getLong(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5)
                        );
        return wine;
    }
}
